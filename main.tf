resource "aws_instance" "example" {
  ami           = "ami-07b5c2e394fccab6e"
  instance_type = "t2.micro"
  associate_public_ip_address = true 

  tags = {
    Name = "MyEC2Instance"
  }

  user_data = <<-EOF
              #!/bin/bash
              echo 'ssh-rsa ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDImpp3L713XfUDhn2lfNq/2wsNkL2sm1mw88nHYnoS2y4ERDI38QEpvCRaVmEQ4o1zsU/KmI0vHpAiI7Vd/jUSXNOhtjzIgxFXYfPz1brlUUkeWPFId+8emcfB2trmun58HPpqqIHidkBFr8MX/G+OHN7IdZGSNsuUbq62ytSb3BjeASWAqwSlFFgGd5GeAKSztUj1EBaRVG6Mf+TNcd/RviaCcQ0tCoWXpsvIxkN1QqUH99aRRwuQW70rFDslrJ06FQAOjqw5bOk7rwjfKukWNCyCXhHPsWgy6kdRTRuskneYaNsS2yu4ILsbPU+e0sprcoGz3foxnml0Cm/9Ak/+nbp5k4i1/kTZq5NJ73OKJNJdC/dQQD/UYGsp5rskR7n9myWDiCOWVdTXSm4EN7rmaYbvyFFrXeMNXzDLLe+3LveoljWr1fTSq6Hop1GPn9ZRqMc4jLbqa3zq6DOikeaYJDWJOAi9EimN6bOuYKgzshTFgEnFN7UJDcYJvTv8TUc=' >> /home/ec2-user/.ssh/authorized_keys
              EOF

  security_groups = [aws_security_group.allow_ssh.name]
}
